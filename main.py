'''
My code.
'''
from threading import *
from socket import *

serverIP = '192.187.121.98'#'192.168.5.3'
serverPort = 26000

clientIP = '' # Anywhere
clientPort = 26000 # Where information is coming from 

realClientIP = ''
realClientPort = 26000

#mainSock = socket(AF_INET, SOCK_DGRAM) # my PC <-> server
#mainSock.bind((clientIP, clientPort))

#while True:
#    message, address = mainSock.recvfrom(1024) # This is blocking btw
#    print(message)
#    print(address) # Adress is the address of the socket _sending the data_
#    
#    out_sock = socket(AF_INET, SOCK_DGRAM) # make a new socket for the outgoing packer
#    out_sock.sendto(message, (serverIP, serverPort))

    
class Proxy2Server(Thread): # Proxy <-> Server
    def __init__(self, host, port):
        super(Proxy2Server, self).__init__()
        self.game_sock = None
        self.host = host
        self.port = port

        self.right_sock = socket(AF_INET, SOCK_DGRAM)
        #self.right_sock.bind((host, port))
        print("Server starting...")
    
    def run(self):
        print("Server running.")
        while True:
            packet, address = self.right_sock.recvfrom(4096)
            print("Server[{}: {}]".format(address, packet))#"[client] "+packet)
            self.game_sock.sendto(packet, (realClientIP, realClientPort)) # Forward the traffic through

class Proxy2Client(Thread): # Proxy <-> Client
    def __init__(self, host, port):
        super(Proxy2Client, self).__init__()
        self.server_sock = None
        self.host = host
        self.port = port
        self.client_port = None

        self.left_sock = socket(AF_INET, SOCK_DGRAM)
        self.left_sock.bind((host, port))
        print("Starting client...")

    def run(self):
        print("Client running.")
        while True:
            packet, address = self.left_sock.recvfrom(4096)
            print("Client[{}: {}]".format(address, packet))#"[client] "+packet)

            realClientIP = address[0]
            realClientPort = address[1]

            self.server_sock.sendto(packet, (serverIP, serverPort)) # Forward the traffic after printing it


p2s = Proxy2Server(serverIP, serverPort)
p2c = Proxy2Client(clientIP, clientPort)
p2s.game_sock = p2c.left_sock
p2c.server_sock = p2s.right_sock

p2c.start()
p2s.start()
